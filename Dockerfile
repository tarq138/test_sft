FROM python:3.9-slim

ARG APPMODE=prod

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV APPMODE=$APPMODE

COPY requirements.txt .
COPY sft/ /app/

RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements.txt

WORKDIR /app

HEALTHCHECK --interval=1m --timeout=5s --retries=2 --start-period=10s \
  CMD wget -qO- http://0.0.0.0:8000/healthcheck/ || exit 1