from django.urls import path

from .api.views import (
    CreditApplicationView,
    ContractView,
    ContractDetailView,
    ManufacturerView,
    ProductView
)

app_name = 'bank_app'

urlpatterns = [
    path('credit_application/', CreditApplicationView.as_view()),
    path('contract/', ContractView.as_view()),
    path('contract/<int:id>', ContractDetailView.as_view()),
    path('manufacturer/', ManufacturerView.as_view()),
    path('product/', ProductView.as_view())
]