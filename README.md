# Test SFT
Test task from SFT Company.

### Launching the application
> docker compose up -d

OR
> docker-compose up -d

#### Credentials from admin panel
- `admin`: login
- `admin`: password for **admin** user


### API
1. [GET] `*/api/v1.0/bank/credit_application` - Get all credit applications
2. [GET] `*/api/v1.0/bank/contract` - Get all contracts
3. [GET] `*/api/v1.0/bank/contract/{id}` - Get all manufacturer id from credit application from contract
4. [GET] `*/api/v1.0/bank/manufacturer` - Get all manufacturer
5. [GET] `*/api/v1.0/bank/product` - Get all products