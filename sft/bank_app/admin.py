from django.contrib import admin

from bank_app.models import (
    CreditApplication,
    Contract,
    Manufacturer,
    Product
)


@admin.register(CreditApplication)
class CreditApplicationAdmin(admin.ModelAdmin):
    list_display = ('name', 'display_products', 'created_date')
    search_fields = ('name',)
    ordering = ('created_date',)


@admin.register(Contract)
class ContractAdmin(admin.ModelAdmin):
    list_display = ('name', 'display_credit_application', 'display_products', 'created_date')
    search_fields = ('name',)
    ordering = ('created_date',)


@admin.register(Manufacturer)
class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_date')
    search_fields = ('name',)
    ordering = ('created_date',)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'display_credit_application', 'display_manufacturer', 'created_date']
    search_fields = ('name',)
    ordering = ('created_date',)
