from django.db import models
from django.contrib import admin


class CreditApplication(models.Model):
    name = models.CharField(max_length=100, unique=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    @admin.display(description='Products')
    def display_products(self):
        return ', '.join(product.name for product in Product.objects.filter(credit_application=self))


class Contract(models.Model):
    name = models.CharField(max_length=100, unique=True)
    credit_application = models.OneToOneField(CreditApplication, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    @admin.display(description='Credit Application')
    def display_credit_application(self):
        return self.credit_application.name

    @admin.display(description='Products')
    def display_products(self):
        return ', '.join(product.name for product in Product.objects.filter(credit_application=self.credit_application))


class Manufacturer(models.Model):
    name = models.CharField(max_length=100, unique=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=100, unique=True)
    credit_application = models.ForeignKey(CreditApplication, on_delete=models.CASCADE)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ['credit_application', 'name']

    @admin.display(description='Credit Application')
    def display_credit_application(self):
        return self.credit_application.name

    @admin.display(description='Manufacturer')
    def display_manufacturer(self):
        return self.manufacturer.name
