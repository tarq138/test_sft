from rest_framework.response import Response
from rest_framework.views import APIView

from bank_app.models import (
    CreditApplication,
    Contract,
    Manufacturer,
    Product
)
from bank_app.api.serializers import (
    CreditApplicationSerializer,
    ContractSerializer,
    ManufacturerSerializer,
    ProductSerializer
)


class CreditApplicationView(APIView):
    def get(self, request):
        credit_applications = CreditApplication.objects.all()
        serializer = CreditApplicationSerializer(credit_applications, many=True)
        return Response(serializer.data)


class ContractView(APIView):
    def get(self, request):
        contracts = Contract.objects.all()
        serializer = ContractSerializer(contracts, many=True)
        return Response(serializer.data)


class ContractDetailView(APIView):
    def get(self, request, id):
        return Response(set(Product.objects.filter(credit_application__contract__id=id).values_list('manufacturer__id', flat=True)))


class ManufacturerView(APIView):
    def get(self, request):
        manufacturers = Manufacturer.objects.all()
        serializer = ManufacturerSerializer(manufacturers, many=True)
        return Response(serializer.data)


class ProductView(APIView):
    def get(self, request):
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)
